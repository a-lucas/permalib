module permaculture

go 1.14

require (
	github.com/a8m/entsize v0.0.0-20200831122419-62e79c940aa9 // indirect
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/facebook/ent v0.5.4
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/inconshreveable/log15 v0.0.0-20201112154412-8562bdadbbac // indirect
	github.com/jonlaing/htmlmeta v0.0.0-20151027182219-aa8670c4f78c
	github.com/kkdai/youtube/v2 v2.3.0 // indirect
	github.com/lib/pq v1.9.0
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/revel/cmd v1.0.3 // indirect
	github.com/revel/config v1.0.0 // indirect
	github.com/revel/modules v1.0.0
	github.com/revel/revel v1.0.0
	golang.org/x/mod v0.4.1 // indirect
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	golang.org/x/tools v0.0.0-20210115202250-e0d201561e39 // indirect
	google.golang.org/api v0.36.0
)

package main

import (
	"context"
	"fmt"
	"github.com/kkdai/youtube/v2"
	_ "github.com/lib/pq"
	"io"
	"log"
	"os"
	"os/exec"
	"permaculture/common"
	"permaculture/ent"
	"permaculture/ent/video"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

func videoPath(videoId string) string {
	return common.VideoRoot + "/" + videoId
}

func createVideoPath(videoId string) error {
	return os.Mkdir(videoPath(videoId), os.ModePerm)
}

func getBestVideo(videos youtube.FormatList) youtube.Format {
	for _, f := range videos {
		if strings.Contains(f.QualityLabel, "1080p") && strings.Contains(f.MimeType, "vp9") && len(f.ApproxDurationMs) > 0{
			return f
		}
	}
	m := mapVideoFormat()
	sort.Slice(videos, func(i, j int) bool {
		return m[videos[i].QualityLabel] > m[videos[j].QualityLabel]
	})
	fmt.Println("Video list")
	for _, f := range videos {
		fmt.Printf("Quality: %s Size %s Mime: %s, Qualitylabel: %s\n", f.Quality, f.ContentLength, f.MimeType, f.QualityLabel)
		if strings.Contains(f.QualityLabel, "1080p") && len(f.ApproxDurationMs) > 0 {
			return f
		}
	}
	for _, f := range videos {
		if  len(f.ApproxDurationMs) > 0 {
			return f
		}
	}
	return videos[0] // this will FAIL
}

func mapVideoFormat() map[string]int {
	m := make(map[string]int)
	m["144p"] =0
	m["240p"] =1
	m["360p"] =2
	m["480p"] =3
	m["720p"] =4
	m["1080p"] =5
	m["1080p60"] =6
	return m
}

/*
If BOTH has vp9 With 1080p quality, then take it
Select Best Audio


Select Best 1080p Video
- vp9
- other


 */
func hasAudio(format youtube.Format) bool { return len(format.AudioQuality) >0}
func hasVideo(format youtube.Format) bool { return len(format.QualityLabel) > 0 && strings.Contains(format.MimeType, "video/")}

// returns the best video/audio combo to download
// also returns the best both
func SelectFormats(formats youtube.FormatList) (youtube.FormatList, youtube.Format) {
	audio := make(youtube.FormatList, 0)
	videoList := make(youtube.FormatList, 0)
	both  := make(youtube.FormatList, 0)

	for _, format := range  formats {
		if hasVideo(format) && hasAudio(format) {
			both = append(both, format)
		} else if hasAudio(format) {
			// only add audio only files
			if strings.Contains(format.MimeType, "audio/") {
				audio = append(audio, format)
			}
		} else {
			videoList = append(videoList, format)
		}
	}

	fmt.Printf("Both: %d, Audio: %d, Video: %d\n", len(both), len(audio), len(videoList))
	m := mapVideoFormat()
	sort.Slice(both, func(i, j int) bool {
		return m[both[i].QualityLabel] > m[both[j].QualityLabel]
	})

	fmt.Println("Both: ")
	for _, f := range both {
		fmt.Printf("Quality: %s Size %s Mime: %s, Qualitylabel: %s\n", f.Quality,f.ContentLength,  f.MimeType, f.QualityLabel)
		if f.QualityLabel == "1080p" && strings.Contains(f.MimeType, "vp9"){
			return youtube.FormatList{f},getBestVideo(both)
		}
	}

	// Edge case
	if len(audio) == 0 || len(videoList) == 0 {
		if len(both) == 0 {
			panic("No format available for this format, bug upstream")
		}
		return youtube.FormatList{getBestVideo(both)}, getBestVideo(both)
	}

	bestVideo := getBestVideo(videoList)

	if bestVideo.QualityLabel == both[0].QualityLabel{
		// take both then
		return youtube.FormatList{getBestVideo(both)}, getBestVideo(both)
	}
	// best audio is the biggest always
	sort.Slice(audio, func(i, j int) bool {
		li, _ := strconv.Atoi(audio[i].ContentLength)
		lj, _ := strconv.Atoi(audio[j].ContentLength)
		return li>lj
	})

	return youtube.FormatList{bestVideo, audio[0]}, getBestVideo(both)
}

func download(video *youtube.Video, vid *ent.Video, selectedFormat youtube.Format, fileName string) error {
	client := youtube.Client{}

	durationMs , _ := strconv.Atoi(selectedFormat.ApproxDurationMs)
	dur := time.Duration(time.Duration(durationMs) * time.Millisecond)
	fmt.Printf("%s -- %s %s -- %s\n", dur.String(), selectedFormat.QualityLabel, selectedFormat.AudioQuality, selectedFormat.MimeType)
	start := time.Now()
	resp, err := client.GetStream(video, &selectedFormat)
	if err != nil {
		fmt.Println("Error downloading stream: ", err.Error())
		return err
	}
	createVideoPath(vid.VideoUID)
	file, err := os.Create(common.VideoRoot + "/" + vid.VideoUID + "/"+fileName)
	if err != nil {
		panic(err)
	}
	_, err = io.Copy(file, resp.Body)
	if err != nil {
		fmt.Println("Error copying stream: ", err.Error())
		return err
	}
	if err := resp.Body.Close(); err != nil {
		fmt.Println("Error copying stream: ", err.Error())
		return err
	}
	if err := file.Close(); err != nil {
		fmt.Println("Error copying stream: ", err.Error())
		return err
	}
	if hasAudio(selectedFormat) {
		if _, err := vid.Update().SetAudioMime(selectedFormat.MimeType).Save(context.Background()); err !=nil {
			fmt.Println("Error saving audio type: ", err.Error())
			return err
		}
	}
	if hasVideo(selectedFormat) {
		if _, err := vid.Update().SetVideoMime(selectedFormat.MimeType).Save(context.Background()); err !=nil {
			fmt.Println("Error saving audio type: ", err.Error())
			return err
		}
	}
	fmt.Println(fmt.Sprintf("Video %s content-length = %s done in %s", vid.VideoUID, selectedFormat.ContentLength, time.Now().Sub(start).String()))
	return nil
}


func main() {
	// Loop Trough All the video folder, and those who are setup as downloading , add a started timestamp file
	concurrency := 1
	db, err := ent.Open("postgres", common.PostgresDSN)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	if err := db.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}


	//err = db.Video.Update().
	//	SetDownloading(true).
	//	SetDownloadStarted(false).
	//	SetDownloaded(false).
	//	SetAudioMime("").
	//	SetVideoMime("").
	//	SetNbFiles(0).
	//	SetTimeSpent(0).
	//	Where(video.Or(video.Downloading(true), video.Downloaded(true), video.DownloadStarted(true))).Exec(context.Background())
	//if err !=nil {
	//	fmt.Println(err.Error())
	//	panic(err)
	//}

	ch := make(chan *ent.Video, 0)
	go func() {
		if vids, err := db.Video.Query().Where(video.And(video.DownloadStarted(false), video.Downloading(true), video.Downloaded(false))).All(context.Background()); err != nil {
			log.Panic(err)
		} else {
			for _, vid := range vids {
				ch <- vid
			}
			close(ch)
		}
	}()
	var wg sync.WaitGroup
	wg.Add(concurrency)
	for i := 0; i < concurrency; i++ {
		go func() {
			for {
				if vid, more := <-ch; more {
					client := youtube.Client{}
					video, err := client.GetVideo(vid.VideoUID)
					if err != nil {
						fmt.Println("Error getting video from Youtube: ", err.Error())
						continue
					}
					fmt.Println()
					formatsToDownload, bestEncodedVideo := SelectFormats(video.Formats)

					hasError := false
					bestVideo := false

					for index, selectedFormat := range formatsToDownload  {
						if err := download(video, vid, selectedFormat, "video_"+strconv.Itoa(index)+".mp4"); err !=nil {
							// What to do?
							fmt.Println("Switching to downloading one video with both audio & video")
							if err := download(video, vid, bestEncodedVideo, "video.mp4"); err !=nil  {
								fmt.Println("Error getting video from Youtube: ", err.Error())
							} else {
								bestVideo = true
							}
							continue
						}
					}

					//ffmpeg -i video_0.mp4 -i video_1.mp4 -preset ultrafast -c:v copy -c:a copy -framerate 30  video.mp4

					setCompleted := func() {
						_, err = vid.Update().SetNbFiles(len(formatsToDownload)).SetDownloading(false).SetDownloaded(true).Save(context.Background())
						if err != nil {
							panic(err)
						}
					}

					if len(formatsToDownload) ==2 && !hasError {
						args := []string{
							"-i", common.VideoRoot + "/" + vid.VideoUID + "/video_0.mp4",
							"-i", common.VideoRoot + "/" + vid.VideoUID + "/video_1.mp4",
							"-preset", "ultrafast",
							"-c:v", "copy",
							"-c:a", "copy",
							"-y",
							"-framerate", "30",
							"-strict","-2",
							common.VideoRoot + "/" + vid.VideoUID + "/video.mp4",
						}
						cmd := exec.Command("/usr/bin/ffmpeg", args...)
						if err := cmd.Run(); err !=nil {
							fmt.Println("Error Executing Video: Arguments: ", strings.Join(args, " "))
						} else {
							setCompleted()
						}
					} else {
						if len(formatsToDownload) ==2 && hasError && bestVideo {
							setCompleted()
						} else if len(formatsToDownload) == 1 && !hasError {
							setCompleted()
						}
					}

				} else {
					wg.Done()
					return
				}
			}
		}()
	}
	wg.Wait()
}

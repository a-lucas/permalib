package main

import (
	"context"
	"encoding/base64"
	"fmt"
	_ "github.com/lib/pq"
	"image"
	"image/png"
	"log"
	"os"
	"permaculture/common"
	"permaculture/ent"
	"strconv"
	"strings"
)

func main() {
	db, err := ent.Open("postgres", common.PostgresDSN)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	if err := db.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	ss, err := db.ScreenShot.Query().All(context.Background())

	for _, s := range ss {

		imageReader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(s.ImageBase64[22:]))
		pngImage, _, err := image.Decode(imageReader)
		if err != nil {
			fmt.Println(s.ImageBase64[22:][0:50])
			panic(err)
		}

		if imgFile, err := os.Create(common.ImageRoot+"/"+strconv.Itoa(s.ID)+".png"); err != nil {
			panic(err)
		} else {
			err = png.Encode(imgFile, pngImage)
			if err !=nil {
				panic(err)
			}
			imgFile.Close()
		}

		// Doesnt work
		//unBased, err := base64.StdEncoding.DecodeString(s.ImageBase64)
		//if err != nil {
		//	fmt.Println("Cannot decode b64")
		//	fmt.Println(s.ImageBase64[0:50])
		//	panic("err")
		//}
		//
		//r := bytes.NewReader(unBased)
		//im, err := png.Decode(r)
		//if err != nil {
		//	panic("Bad png")
		//}

		//f, err := os.OpenFile(common.ImageRoot+strconv.Itoa(s.ID)+".png", os.O_WRONLY|os.O_CREATE, 0777)
		//if err != nil {
		//	panic("Cannot open file")
		//}
		//
		//if err := png.Encode(f, im); err != nil {
		//	panic("cannot encode into file")
		//}
		//f.Close()
		//fmt.Print(".")
	}
}

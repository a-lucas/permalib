package models

import (
	"context"
	"google.golang.org/api/googleapi/transport"
	"google.golang.org/api/youtube/v3"
	"net/http"
	"permaculture/common"
	"permaculture/ent"
	"permaculture/ent/video"
)

type YoutubeResult struct {
	Downloading bool           `json:"downloading"`
	Downloaded  bool           `json:"downloaded"`
	Item        *youtube.Video `json:"item"`
}


func GetOrCreateVideo(youtubeId string, con *ent.Client) (*ent.Video, error) {
	if vid, err := con.Video.Query().Where(video.VideoUID(youtubeId)).Only(context.Background()); err !=nil {
		client := &http.Client{
			Transport: &transport.APIKey{Key: common.DeveloperKey},
		}
		service, err := youtube.New(client)
		if err != nil {
			return nil, err
		}

		if details, err := service.Videos.List([]string{"snippet", "contentDetails", "statistics", "id", "player", "topicDetails"}).Id(youtubeId).Do(); err != nil {
			return nil, err
		} else {
			if vid, err := con.Video.Create().
				SetDownloaded(false).
				SetDownloading(false).
				SetVideoUID(youtubeId).
				SetVideo(details.Items[0]).Save(context.Background()); err != nil {
				return nil, err
			} else {
				return vid, nil
			}
		}
	} else {
		return vid, nil
	}
}
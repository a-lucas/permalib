package models

import (
	"context"
	"google.golang.org/api/googleapi/transport"
	"google.golang.org/api/youtube/v3"
	"net/http"
	"permaculture/common"
	"permaculture/ent"
	"permaculture/ent/channel"
)



func GetOrCreateChannel(youtubeId string, con *ent.Client) (*ent.Channel, error) {
	client := &http.Client{
		Transport: &transport.APIKey{Key: common.DeveloperKey},
	}
	service, err := youtube.New(client)
	if err != nil {
		return nil, err
	}

	if channel, err := con.Channel.Query().Where(channel.YoutubeID(youtubeId)).Only(context.Background()); err !=nil {
		// Channel doesn't exists
		// Make Google Query for Channel
		call := service.Channels.List([]string{"brandingSettings","status","contentDetails","id","snippet","topicDetails","statistics","contentDetails","contentOwnerDetails"}).
			Id(youtubeId)
		if youtubeChannelData, err := call.Do(); err != nil {
			return nil, err
		} else {
			return con.Channel.Create().SetName(youtubeChannelData.Items[0].Snippet.Title).SetSynced(false).SetYoutube(youtubeChannelData.Items[0]).SetYoutubeID(youtubeId).Save(context.Background())
		}
	} else {
		return channel, nil
	}
}

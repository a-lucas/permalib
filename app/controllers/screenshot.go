package controllers

import (
	"context"
	"encoding/base64"
	"github.com/davecgh/go-spew/spew"
	"github.com/revel/revel"
	"image"
	"image/png"
	"log"
	"os"
	"permaculture/common"
	"permaculture/ent"
	"permaculture/ent/video"
	"strconv"
	"strings"
)

type ScreenShot struct {
	*revel.Controller
	con *ent.Client
}

func (c *ScreenShot) loadConn() error {
	if c.con != nil {
		return nil
	}
	c.Log.Info("authenticating postgresql")
	if client, err := ent.Open("postgres", common.PostgresDSN); err != nil {
		c.Log.Error(err.Error())
		return err
	} else {
		c.con = client
		if err := c.con.Schema.Create(context.Background()); err != nil {
			log.Fatalf("failed creating schema resources: %v", err)
		}
		return nil
	}
}

func (c *ScreenShot) Save() revel.Result {
	c.loadConn()
	var jsonData map[string]interface{}
	c.Params.BindJSON(&jsonData)
	if screenshot, err := c.con.ScreenShot.Get(context.Background(), int(jsonData["id"].(float64))); err != nil {
		return c.RenderError(err)
	} else {
		spew.Dump(jsonData["legend"])
		spew.Dump(jsonData["description"])
		screenshot.Update().SetLegend(jsonData["legend"].(string)).SetDescription(jsonData["description"].(string)).SaveX(context.Background())
		return c.RenderJSON(screenshot)
	}
}

func (c *ScreenShot) AddPlant() revel.Result {
	c.loadConn()

	plantId, err := strconv.Atoi(c.Params.Query.Get("plant_id"))
	if err !=nil {
		return c.RenderError(err)
	}
	screenshotId, err := strconv.Atoi(c.Params.Query.Get("screenshot_id"))
	if err !=nil {
		return c.RenderError(err)
	}
	pl, err := c.con.Plant.Get(context.Background(), plantId)
	if err !=nil {
		return c.RenderError(err)
	}
	_, err = pl.Update().AddScreenshotIDs(screenshotId).Save(context.Background())
	if err !=nil {
		return c.RenderError(err)
	}

	screenshot, err := c.con.ScreenShot.Get(context.Background(), screenshotId)
	if err !=nil {
		return c.RenderError(err)
	}
	plants, err := screenshot.QueryPlant().All(context.Background())
	if err !=nil {
		return c.RenderError(err)
	}
	return c.RenderJSON(plants)
}

func (c *ScreenShot) RemovePlant() revel.Result {
	c.loadConn()

	plantId, err := strconv.Atoi(c.Params.Query.Get("plant_id"))
	if err !=nil {
		return c.RenderError(err)
	}
	screenshotId, err := strconv.Atoi(c.Params.Query.Get("screenshot_id"))
	if err !=nil {
		return c.RenderError(err)
	}
	pl, err := c.con.Plant.Get(context.Background(), plantId)
	if err !=nil {
		return c.RenderError(err)
	}
	_, err = pl.Update().RemoveScreenshotIDs(screenshotId).Save(context.Background())
	if err !=nil {
		return c.RenderError(err)
	}

	screenshot, err := c.con.ScreenShot.Get(context.Background(), screenshotId)
	if err !=nil {
		return c.RenderError(err)
	}
	plants, err := screenshot.QueryPlant().All(context.Background())
	if err !=nil {
		return c.RenderError(err)
	}
	return c.RenderJSON(plants)
}

func (c *ScreenShot) Delete() revel.Result {
	c.loadConn()
	screenshotId, err := strconv.Atoi(c.Params.Query.Get("screenshot_id"))
	if err !=nil {
		return c.RenderError(err)
	}
	if err := c.con.ScreenShot.DeleteOneID(screenshotId).Exec(context.Background()); err !=nil {
		return c.RenderError(err)
	} else {
		return c.RenderJSON(screenshotId)
	}
}

func (c *ScreenShot) AddMeasurement() revel.Result {
	c.loadConn()
	var jsonData map[string]interface{}
	//jsonData := &ent.Measurement{}
	c.Params.BindJSON(&jsonData)
	c.Log.Info("GOING TO DUMP")
	spew.Dump(jsonData)
	if m, err := c.con.Measurement.Create().SetX1(int(jsonData["x1"].(float64))).
		SetX2(int(jsonData["x2"].(float64))).
		SetY1(int(jsonData["y1"].(float64))).
		SetY2(int(jsonData["y2"].(float64))).SetName("").SetScreenshotID(int(jsonData["screenshot_id"].(float64))).Save(context.Background()); err !=nil {
			return c.RenderError(err)
	} else {
		return c.RenderJSON(m)
	}
	//return c.RenderJSON([]*ent.Measurement{jsonData})
}

func (c *ScreenShot) SaveMeasurement() revel.Result {
	c.loadConn()
	var jsonData map[string]interface{}
	//jsonData := &ent.Measurement{}
	c.Params.BindJSON(&jsonData)
	c.Log.Info("GOING TO DUMP")
	spew.Dump(jsonData)

	name := ""
	if n, ok := jsonData["name"].(string); ok {
		name = n
	}
	m, err := c.con.Measurement.Get(context.Background(), int(jsonData["id"].(float64)))
	if err !=nil {
		return c.RenderError(err)
	}
	if _, err := m.Update().SetX1(int(jsonData["x1"].(float64))).
		SetX2(int(jsonData["x2"].(float64))).
		SetY1(int(jsonData["y1"].(float64))).
		SetY2(int(jsonData["y2"].(float64))).SetName(name).Save(context.Background()); err !=nil {
		return c.RenderError(err)
	} else {
		return c.RenderJSON(m)
	}
}

func (c *ScreenShot) DeleteMeasurement() revel.Result {
	c.loadConn()
	jsonData := &ent.Measurement{}
	c.Params.BindJSON(&jsonData)
	return c.RenderJSON([]*ent.Measurement{jsonData})
}

func (c *ScreenShot) New() revel.Result {
	c.loadConn()

	var jsonData map[string]interface{}
	c.Params.BindJSON(&jsonData)

	videoId, ok := jsonData["videoId"].(string)
	if !ok  {
		spew.Dump(jsonData)
		panic("SOME SHIT")
	}

	vidId, err := c.con.Video.Query().Where(video.VideoUID(videoId)).OnlyID(context.Background())
	if err != nil {
		return c.RenderError(err)
	}




	if screenshot, err := c.con.ScreenShot.
		Create().
		SetVideoUID(jsonData["videoId"].(string)).
		SetCurrentTime(jsonData["time"].(float64)).
		SetLegend("").
		SetDescription("").
		SetVideoID(vidId).
		Save(context.Background()); err != nil {
		return c.RenderError(err)
	} else {

		imageReader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(jsonData["base64ImageData"].(string)[22:]))
		pngImage, _, err := image.Decode(imageReader)
		if err != nil {
			c.Log.Error(jsonData["base64ImageData"].(string)[22:][0:50])
			c.con.ScreenShot.DeleteOneID(screenshot.ID)
			return c.RenderError(err)
		}

		if imgFile, err := os.Create(common.ImageRoot+"/"+strconv.Itoa(screenshot.ID)+".png"); err != nil {
			return c.RenderError(err)
		} else {
			err = png.Encode(imgFile, pngImage)
			if err !=nil {
				c.con.ScreenShot.DeleteOneID(screenshot.ID)
				return c.RenderError(err)
			}
			imgFile.Close()
		}


		return c.RenderJSON(screenshot)
	}
}

package controllers

import (
	"context"
	"github.com/revel/revel"
	"log"
	"permaculture/common"
	"permaculture/ent"
)

type Farm struct {
	*revel.Controller
	con *ent.Client
}

func (c *Farm) loadConn() error {
	if c.con != nil {
		return nil
	}
	c.Log.Info("authenticating postgresql")
	if client, err := ent.Open("postgres", common.PostgresDSN); err != nil {
		c.Log.Error(err.Error())
		return err
	} else {
		c.con = client
		if err := c.con.Schema.Create(context.Background()); err != nil {
			log.Fatalf("failed creating schema resources: %v", err)
		}
		return nil
	}
}

func (c *Farm) All() revel.Result {
	c.loadConn()
	if farms, err := c.con.Farm.Query().WithFarmIndividuals().WithVideos().WithLinks().All(context.Background()); err !=nil {
		return c.RenderError(err)
	} else {
		return c.RenderJSON(farms)
	}
}

func (c *Farm) New() revel.Result {
	c.loadConn()
	var body map[string]interface{}
	if err := c.Params.BindJSON(&body); err !=nil {
		return c.RenderError(err)
	}
	if farm, err := c.con.Farm.Create().SetLocation("").SetFarmName(body["farm_name"].(string)).Save(context.Background()); err !=nil {
		return c.RenderError(err)
	} else {
		return c.RenderJSON(farm)
	}
}

func (c *Farm) Save() revel.Result {
	c.loadConn()
	var body map[string]interface{}
	if err := c.Params.BindJSON(&body); err !=nil {
		return c.RenderError(err)
	}
	if farm, err := c.con.Farm.Get(context.Background(), int(body["id"].(float64))); err !=nil {
		return c.RenderError(err)
	} else {
		if farm, err := farm.Update().SetFarmName(body["farm_name"].(string)).SetLocation(body["location"].(string)).Save(context.Background()); err !=nil {
			return c.RenderError(err)
		} else {
			return c.RenderJSON(farm)
		}
	}
}


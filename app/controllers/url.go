package controllers

import (
	"fmt"
	"github.com/jonlaing/htmlmeta"
	"github.com/revel/revel"
	"net/http"
)

type Url struct {
	*revel.Controller
}

/*
{
    "success" : 1,
    "meta": {
        "title" : "CodeX Team",
        "description" : "Club of web-development, design and marketing. We build team learning how to build full-valued projects on the world market.",
        "image" : {
            "url" : "https://codex.so/public/app/img/meta_img.png"
        }
    }
}
*/
func (c Url) PreviewLink() revel.Result {

	type img struct {
		Url string `json:"url"`
	}

	type meta struct {
		Link        string `json:"link"`
		Title       string `json:"title"`
		Description string `json:"description"`
		Image img `json:"image"`
	}


	type resp struct {
		Success int  `json:"success"`
		Meta    meta `json:"meta"`
	}

	response, err := http.Get(c.Params.Query.Get("url"))
	if err != nil {
		return c.RenderError(err)
	} else {
		defer response.Body.Close()
		data := htmlmeta.Extract(response.Body)
fmt.Println(data.OGImage)
		return c.RenderJSON(resp{1, meta{
			Link:        c.Params.Query.Get("url"),
			Title:       data.Title,
			Description: data.Description,
			Image:img{Url:data.OGImage},
		}})
	}

}

package controllers

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"permaculture/app/models"
	"permaculture/common"
	"permaculture/ent"
	"permaculture/ent/search"
	"time"

	_ "github.com/lib/pq"
	"github.com/revel/revel"
	"google.golang.org/api/googleapi/transport"
	"google.golang.org/api/youtube/v3"
)

type Search struct {
	*revel.Controller
	con *ent.Client
}

func (c *Search) loadConn() error {
	if c.con != nil {
		return nil
	}
	c.Log.Info("authenticating postgresql")
	if client, err := ent.Open("postgres", common.PostgresDSN); err != nil {
		c.Log.Error(err.Error())
		return err
	} else {
		c.con = client
		//if err := c.con.Schema.Create(context.Background()); err != nil {
		//	log.Fatalf("failed creating schema resources: %v", err)
		//}
		return nil
	}
}

func (c Search) All() revel.Result {
	c.loadConn()
	if searches, err := c.con.Search.Query().All(context.Background()); err !=nil {
		return c.RenderError(err)
	} else {
		return c.RenderJSON(searches)
	}
}

func (c Search) Make() revel.Result {
	query := c.Params.Values.Get("keyword")
	channelStr := c.Params.Values.Get("channel")
	var maxResults int64 = 50
	ctx, _ := context.WithCancel(context.Background())
	if err := c.loadConn(); err != nil {
		return c.RenderError(err)
	}
	client := &http.Client{
		Transport: &transport.APIKey{Key: common.DeveloperKey},
	}
	service, err := youtube.New(client)
	if err != nil {
		return c.RenderError(err)
	}
	var response *youtube.SearchListResponse
	if results, err := c.con.Search.Query().Where(search.SearchQuery(query), search.SearchChannel(channelStr)).All(ctx); err != nil {
		c.Log.Error(err.Error())
		return c.RenderError(err)
	} else {
		if len(results) == 1 {
			fmt.Println("SEARCH ALREADY SAVED, Saving 100 UNIT credit")
			response = results[0].Result
		} else {

			// Make the API call to YouTube.
			call := service.Search.List([]string{"id", "snippet"}).
				Q(query).
				ChannelId(channelStr).
				MaxResults(maxResults)

			if resp, err := call.Do(); err != nil {
				c.Log.Error(err.Error())
				return c.RenderError(err)
			} else {
				response = resp
				if _, err := c.con.Search.Create().SetSearchChannel(channelStr).SetSearchQuery(query).SetSearchDate(time.Now()).SetResult(resp).Save(ctx); err != nil {
					c.Log.Error(err.Error())
					return c.RenderError(err)
				}
			}
		}
	}

	var searchedChannel *ent.Channel
	if len(channelStr) > 0 && len(query) == 0 {
		if sc, err := models.GetOrCreateChannel(channelStr, c.con); err != nil {
			c.Log.Error(err.Error())
			return c.RenderError(err)
		} else {
			searchedChannel = sc
		}
	}

	// Find All the IDs that are not defined yet
	videoIds := make(map[string]*ent.Video, 0)
	allVideos := make(map[string]*ent.Video, 0)
	allChannels := make(map[string]*ent.Channel, 0)

	fmt.Println("Loading search in memory")
	for _, item := range response.Items {
		if len(item.Id.VideoId) > 0 {

			if video, err := models.GetOrCreateVideo(item.Id.VideoId, c.con); err != nil {
				c.Log.Error(err.Error())
				return c.RenderError(err)
			} else {
				if searchedChannel != nil && !searchedChannel.Synced {
					searchedChannel.Update().AddVideoIDs(video.ID).Save(context.Background())
				}
				channelId := video.Video.Snippet.ChannelId
				if len(channelId) > 0 {
					if videoChannel, err := models.GetOrCreateChannel(channelId, c.con); err != nil {
						c.Log.Error(err.Error())
						return c.RenderError(err)
					} else {
						videoChannel.Update().AddVideoIDs(video.ID).Save(context.Background())
						allChannels[channelId] = videoChannel
					}
				}
				allVideos[video.VideoUID] = video
			}
		}
	}

	if searchedChannel != nil  && !searchedChannel.Synced {
		if _, err := searchedChannel.Update().SetSynced(true).Save(context.Background()); err != nil {
			c.Log.Error(err.Error())
			return c.RenderError(err)
		}
	}

	fmt.Println("Querying missing videos", len(videoIds))

	if len(allVideos) == 0 {
		c.RenderError(errors.New("no results to show up"))
	}
	return c.RenderJSON(allVideos)
}

package controllers

import (
	"context"
	"errors"
	"github.com/revel/revel"
	"log"
	"permaculture/common"
	"permaculture/ent"
	"permaculture/ent/screenshot"
	"permaculture/ent/video"
	"strconv"
	"strings"
)

type Video struct {
	*revel.Controller
	con *ent.Client
}

func (c *Video) loadConn() error {
	if c.con != nil {
		return nil
	}
	c.Log.Info("authenticating postgresql")
	if client, err := ent.Open("postgres", common.PostgresDSN); err != nil {
		c.Log.Error(err.Error())
		return err
	} else {
		c.con = client
		if err := c.con.Schema.Create(context.Background()); err != nil {
			log.Fatalf("failed creating schema resources: %v", err)
		}
		return nil
	}
}

func (c *Video) Load() revel.Result {
	c.loadConn()
	vid, err := c.con.Video.Query().Where(video.VideoUID(c.Params.Query.Get("id"))).Only(context.Background())
	if err != nil {
		return c.RenderError(err)
	}
	return c.RenderJSON(c.loadVid(vid.ID))
}

// param ["ids"]
func (c Video) DownloadMany() revel.Result {
	videos := strings.Split(c.Params.Values.Get("ids"), ",")
	if err := c.loadConn(); err != nil {
		c.Log.Error(err.Error())
		return c.RenderError(err)
	}

	for _, videoId := range videos {
		if result, err := c.con.Video.Query().Where(video.VideoUID(videoId)).All(context.Background()); err != nil {
			return c.RenderError(err)
		} else if len(result) == 0 {
			err := errors.New("video not found")
			return c.RenderError(err)
		} else {
			if _, err := result[0].Update().SetDownloading(true).Save(context.Background()); err != nil {
				return c.RenderError(err)
			}
		}
	}
	return c.RenderJSON(true)
}

// param ["id"]
func (c Video) Download() revel.Result {
	if err := c.loadConn(); err != nil {
		c.Log.Error(err.Error())
		return c.RenderError(err)
	}
	if vid, err := c.con.Video.Query().Where(video.VideoUID(c.Params.Values.Get("id"))).Only(context.Background()); err != nil {
		return c.RenderError(err)
	} else {
		if vid.Update().SetDownloading(true).Save(context.Background()); err != nil {
			return c.RenderError(err)
		} else {
			return c.RenderJSON(c.loadVid(vid.ID))
		}
	}
}

func (c Video) loadVid(id int) *ent.Video {
	c.loadConn()
	vid := c.con.Video.Query().Where(video.ID(id)).WithFarms().WithChannels().WithLinks().OnlyX(context.Background())
	vid.Edges.Screenshots = vid.QueryScreenshots().Order(ent.Asc(screenshot.FieldCurrentTime)).WithMeasurements().WithPlant().WithPlantSpecie().AllX(context.Background())
	return vid
}

func (c Video) MostActive() revel.Result {
	c.loadConn()
	all, err := c.con.Video.Query().Where(video.TimeSpentGT(0)).IDs(context.Background())
	if err !=nil {
		return c.RenderError(err)
	} else {
		results := make([]*ent.Video, 0)
		for _, v := range all {
			results = append(results, c.loadVid(v))
		}
		return c.RenderJSON(results)
	}
}

func (c Video) RecentlyDownloaded() revel.Result {
	c.loadConn()
	all, err := c.con.Video.Query().Where(video.TimeSpent(0)).Order().WithLinks().WithChannels().WithFarms().WithScreenshots().All(context.Background())
	if err !=nil {
		return c.RenderError(err)
	} else {
		return c.RenderJSON(all)
	}
}

func (c Video) TimeSpent() revel.Result {
	c.loadConn()
	videoId, err := strconv.Atoi(c.Params.Query.Get("videoId"))
	if err != nil {
		return c.RenderError(err)
	}
	timeSpent, err := strconv.Atoi(c.Params.Query.Get("timeSpent"))
	if err != nil {
		return c.RenderError(err)
	}
	vid, err := c.con.Video.Get(context.Background(), videoId)
	if err != nil {
		return c.RenderError(err)
	}
	vid, err = vid.Update().SetTimeSpent(timeSpent).Save(context.Background())
	if err != nil {
		return c.RenderError(err)
	}

	return c.RenderJSON(c.loadVid(vid.ID))

}

func (c Video) SetFarm() revel.Result {
	c.loadConn()
	videoId, err := strconv.Atoi(c.Params.Query.Get("videoId"))
	if err != nil {
		return c.RenderError(err)
	}
	farmId, err := strconv.Atoi(c.Params.Query.Get("farmId"))
	if err != nil {
		return c.RenderError(err)
	}
	video, err := c.con.Video.Get(context.Background(), videoId)
	if err != nil {
		return c.RenderError(err)
	}
	video, err = video.Update().SetFarmsID(farmId).Save(context.Background())
	if err != nil {
		return c.RenderError(err)
	}
	farm, err := c.con.Farm.Get(context.Background(), farmId)
	if err != nil {
		return c.RenderError(err)
	}
	type resp struct {
		Video *ent.Video
		Farm  *ent.Farm
	}
	return c.RenderJSON(resp{video, farm})
}

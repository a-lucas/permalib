package controllers

import (
	"context"
	"github.com/revel/revel"
	"log"
	"permaculture/common"
	"permaculture/ent"
	"permaculture/ent/plant"
)

type Plant struct {
	*revel.Controller
	con *ent.Client
}

func (c *Plant) loadConn() error {
	if c.con != nil {
		return nil
	}
	c.Log.Info("authenticating postgresql")
	if client, err := ent.Open("postgres", common.PostgresDSN); err != nil {
		c.Log.Error(err.Error())
		return err
	} else {
		c.con = client
		if err := c.con.Schema.Create(context.Background()); err != nil {
			log.Fatalf("failed creating schema resources: %v", err)
		}
		return nil
	}
}

func (c *Plant) AllPlants() revel.Result {
	c.loadConn()

	type PlantResult struct {
		Name     string
		Id       int
		Children []PlantResult
	}
	results := make([]PlantResult, 0)

	if plants, err := c.con.Plant.Query().Where(plant.Not(plant.HasParent())).All(context.Background()); err != nil {
		return c.RenderError(err)
	} else {

		for _, pl := range plants {

			pr := PlantResult{
				Name:     pl.Name,
				Id:       pl.ID,
				Children: make([]PlantResult, 0),
			}
			categories, _ := pl.QueryPlants().All(context.Background())
			for _, category := range categories {
				pr.Children = append(pr.Children, PlantResult{
					Name:     category.Name,
					Id:       category.ID,
					Children: make([]PlantResult, 0),
				})
			}
			results = append(results, pr)
		}
		return c.RenderJSON(results)
	}
}

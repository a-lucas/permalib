package controllers

import (
	"context"
	"github.com/revel/revel"
	"log"
	"permaculture/common"
	"permaculture/ent"
)

type Channels struct {
	*revel.Controller
	con *ent.Client
}

func (c *Channels) loadConn() error {
	if c.con != nil {
		return nil
	}
	c.Log.Info("authenticating postgresql")
	if client, err := ent.Open("postgres", common.PostgresDSN); err != nil {
		c.Log.Error(err.Error())
		return err
	} else {
		c.con = client
		if err := c.con.Schema.Create(context.Background()); err != nil {
			log.Fatalf("failed creating schema resources: %v", err)
		}
		return nil
	}
}

func (c *Channels) All() revel.Result {
	c.loadConn()
	if channels, err := c.con.Channel.Query().All(context.Background()); err !=nil {
		return c.RenderError(err)
	} else {
		return c.RenderJSON(channels)
	}
}
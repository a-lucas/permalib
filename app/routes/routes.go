// GENERATED CODE - DO NOT EDIT
// This file provides a way of creating URL's based on all the actions
// found in all the controllers.
package routes

import "github.com/revel/revel"


type tStatic struct {}
var Static tStatic


func (_ tStatic) Serve(
		prefix string,
		filepath string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "prefix", prefix)
	revel.Unbind(args, "filepath", filepath)
	return revel.MainRouter.Reverse("Static.Serve", args).URL
}

func (_ tStatic) ServeDir(
		prefix string,
		filepath string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "prefix", prefix)
	revel.Unbind(args, "filepath", filepath)
	return revel.MainRouter.Reverse("Static.ServeDir", args).URL
}

func (_ tStatic) ServeModule(
		moduleName string,
		prefix string,
		filepath string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "moduleName", moduleName)
	revel.Unbind(args, "prefix", prefix)
	revel.Unbind(args, "filepath", filepath)
	return revel.MainRouter.Reverse("Static.ServeModule", args).URL
}

func (_ tStatic) ServeModuleDir(
		moduleName string,
		prefix string,
		filepath string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "moduleName", moduleName)
	revel.Unbind(args, "prefix", prefix)
	revel.Unbind(args, "filepath", filepath)
	return revel.MainRouter.Reverse("Static.ServeModuleDir", args).URL
}


type tTestRunner struct {}
var TestRunner tTestRunner


func (_ tTestRunner) Index(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TestRunner.Index", args).URL
}

func (_ tTestRunner) Suite(
		suite string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "suite", suite)
	return revel.MainRouter.Reverse("TestRunner.Suite", args).URL
}

func (_ tTestRunner) Run(
		suite string,
		test string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "suite", suite)
	revel.Unbind(args, "test", test)
	return revel.MainRouter.Reverse("TestRunner.Run", args).URL
}

func (_ tTestRunner) List(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TestRunner.List", args).URL
}


type tApp struct {}
var App tApp


func (_ tApp) Index(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("App.Index", args).URL
}


type tChannels struct {}
var Channels tChannels


func (_ tChannels) All(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Channels.All", args).URL
}


type tFarm struct {}
var Farm tFarm


func (_ tFarm) All(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Farm.All", args).URL
}

func (_ tFarm) New(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Farm.New", args).URL
}

func (_ tFarm) Save(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Farm.Save", args).URL
}


type tPlant struct {}
var Plant tPlant


func (_ tPlant) AllPlants(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Plant.AllPlants", args).URL
}


type tScreenShot struct {}
var ScreenShot tScreenShot


func (_ tScreenShot) Save(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("ScreenShot.Save", args).URL
}

func (_ tScreenShot) AddPlant(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("ScreenShot.AddPlant", args).URL
}

func (_ tScreenShot) RemovePlant(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("ScreenShot.RemovePlant", args).URL
}

func (_ tScreenShot) Delete(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("ScreenShot.Delete", args).URL
}

func (_ tScreenShot) AddMeasurement(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("ScreenShot.AddMeasurement", args).URL
}

func (_ tScreenShot) SaveMeasurement(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("ScreenShot.SaveMeasurement", args).URL
}

func (_ tScreenShot) DeleteMeasurement(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("ScreenShot.DeleteMeasurement", args).URL
}

func (_ tScreenShot) New(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("ScreenShot.New", args).URL
}


type tSearch struct {}
var Search tSearch


func (_ tSearch) All(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Search.All", args).URL
}

func (_ tSearch) Make(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Search.Make", args).URL
}


type tUrl struct {}
var Url tUrl


func (_ tUrl) PreviewLink(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Url.PreviewLink", args).URL
}


type tVideo struct {}
var Video tVideo


func (_ tVideo) Load(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Video.Load", args).URL
}

func (_ tVideo) DownloadMany(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Video.DownloadMany", args).URL
}

func (_ tVideo) Download(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Video.Download", args).URL
}

func (_ tVideo) MostActive(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Video.MostActive", args).URL
}

func (_ tVideo) RecentlyDownloaded(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Video.RecentlyDownloaded", args).URL
}

func (_ tVideo) TimeSpent(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Video.TimeSpent", args).URL
}

func (_ tVideo) SetFarm(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Video.SetFarm", args).URL
}



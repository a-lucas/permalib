package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"google.golang.org/api/youtube/v3"
	"permaculture/common"
)

// Video holds the schema definition for the Video entity.
type Video struct {
	ent.Schema
}

// Fields of the Video.
func (Video) Fields() []ent.Field {
	return []ent.Field{
		field.String("video_uid").Unique(),
		field.JSON("video", &youtube.Video{}),
		field.Bool("downloaded").Default(false),
		field.Bool("downloading").Default(false),
		field.Bool("download_started").Default(false),
		field.Int("time_spent").Default(0).Optional().Nillable(),
		field.String("video_mime").Default(""),
		field.String("audio_mime").Default(""),
		field.Int("nb_files").Default(0).Optional(),
	}
}

// Edges of the Video.
func (Video) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("screenshots", ScreenShot.Type),
		edge.From("farms", Farm.Type).Ref("videos").Unique(),
		edge.From("channels", Channel.Type).Ref("videos"),
		edge.To("links", Link.Type),
	}
}

func (Video) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}

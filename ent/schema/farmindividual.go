package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"permaculture/common"
)

// FarmIndividual holds the schema definition for the FarmIndividual entity.
type FarmIndividual struct {
	ent.Schema
}

// Fields of the FarmIndividual.
func (FarmIndividual) Fields() []ent.Field {
	return []ent.Field{
		field.String("first_name").NotEmpty(),
		field.String("last_name"),
	}
}

// Edges of the FarmIndividual.
func (FarmIndividual) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("farms", Farm.Type).Ref("farmIndividuals"),
	}
}

func (FarmIndividual) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}

package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"permaculture/common"
)

// ScreenShot holds the schema definition for the ScreenShot entity.
type ScreenShot struct {
	ent.Schema
}

// Fields of the ScreenShot.
func (ScreenShot) Fields() []ent.Field {
	return []ent.Field{
		field.String("video_uid"),
		field.String("legend"),
		field.String("description"),
		field.Float("current_time"),
	}
}

// Edges of the ScreenShot.
func (ScreenShot) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("video", Video.Type).Ref("screenshots").Unique(),
		edge.From("plant_specie", PlantSpecie.Type).Ref("screenshots"),
		edge.From("plant", Plant.Type).Ref("screenshots"),
		edge.To("measurements", Measurement.Type),
	}
}

func (ScreenShot) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}

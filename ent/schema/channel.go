package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"google.golang.org/api/youtube/v3"
	"permaculture/common"
)

// Channel holds the schema definition for the Channel entity.
type Channel struct {
	ent.Schema
}

// Fields of the Channel.
func (Channel) Fields() []ent.Field {
	return []ent.Field{
		field.String("youtube_id"),
		field.String("name"),
		field.Bool("synced"),
		field.JSON("youtube", &youtube.Channel{}),
	}
}

func (Channel) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}
// Edges of the Channel.
func (Channel) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("videos", Video.Type),
	}
}

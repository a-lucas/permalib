package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
)

// Measurement holds the schema definition for the Measurement entity.
type Measurement struct {
	ent.Schema
}

// Fields of the Measurement.
func (Measurement) Fields() []ent.Field {
	return []ent.Field {
		field.Int("x1"),
		field.Int("x2"),
		field.Int("y1"),
		field.Int("y2"),
		field.String("name"),
	}
}

// Edges of the Measurement.
func (Measurement) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("screenshot", ScreenShot.Type).Ref("measurements").Unique().Required(),
	}
}

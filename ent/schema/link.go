package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"permaculture/common"
)

// Video holds the schema definition for the Video entity.
type Link struct {
	ent.Schema
}

// Fields of the Video.
func (Link) Fields() []ent.Field {
	return []ent.Field{
		field.String("url").Unique(),
		field.Enum("type").Values("website", "social", "video"),
	}
}

// Edges of the Video.
func (Link) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("farms", Farm.Type).Ref("links"),
		edge.From("videos", Video.Type).Ref("links"),
	}
}
func (Link) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}

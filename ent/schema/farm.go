package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"permaculture/common"
)

// Farm holds the schema definition for the Farm entity.
type Farm struct {
	ent.Schema
}

// Fields of the Farm.
func (Farm) Fields() []ent.Field {
	return []ent.Field{
		field.String("farm_name"),
		field.String("location"),
	}
}

// Edges of the Farm.
func (Farm) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("farmIndividuals", FarmIndividual.Type),
		edge.To("videos", Video.Type),
		edge.To("links", Link.Type),
	}
}

func (Farm) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}

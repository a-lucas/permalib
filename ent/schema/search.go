package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/field"
	"google.golang.org/api/youtube/v3"
	"permaculture/common"
)

// Make holds the schema definition for the Make entity.
type Search struct {
	ent.Schema
}

// Fields of the Make.
func (Search) Fields() []ent.Field {
	return []ent.Field{
		field.String("search_query"),
		field.String("search_channel"),
		field.JSON("result", &youtube.SearchListResponse{}),
		field.Time("search_date"),
	}
}

// Edges of the Make.
func (Search) Edges() []ent.Edge {
	return nil
}

func (Search) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}

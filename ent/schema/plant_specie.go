package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"permaculture/common"
)

type PlantSpecie struct {
	ent.Schema
}

// Fields of the Farm.
func (PlantSpecie) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").Unique(),
		field.Bool("leaves_eatable").Default(false).Optional(),
		field.Bool("root_eatable").Default(false).Optional(),
		field.Bool("fruit_eatable").Default(false).Optional(),
		field.Bool("rare").Default(false).Optional(),
		field.Int("nb_photos").Default(0),
		field.String("url").Default(""),
	}
}

// Edges of the Farm.
func (PlantSpecie) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("plants", Plant.Type).Ref("species").Unique(),
		edge.To("screenshots", ScreenShot.Type),
	}
}

func (PlantSpecie) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}

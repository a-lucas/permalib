package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"permaculture/common"
)

// Farm holds the schema definition for the Farm entity.
type Plant struct {
	ent.Schema
}

// Fields of the Farm.
func (Plant) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").Unique(),
		field.Text("base64Img").Optional(),
		field.Int("level").Optional(),
	}
}

// Edges of the Farm.
func (Plant) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("species", PlantSpecie.Type),
		edge.To("screenshots", ScreenShot.Type),
		edge.To("plants", Plant.Type).
			From("parent").
			Unique(),
	}
}

func (Plant) Mixin() []ent.Mixin {
	return []ent.Mixin{
		common.TimeMixin{},
	}
}

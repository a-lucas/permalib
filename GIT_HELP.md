**Rebase ALL commits**

`git rebase -i --root`

For each commit except the first, change pick to squash.

** Change git editor**

`git config --global core.editor "nano"`


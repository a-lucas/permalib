package scrapper

import (
	"context"
	"encoding/xml"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
	"os"
	"permaculture/common"
	"permaculture/ent"
	"permaculture/ent/plant"
	"permaculture/ent/plantspecie"
	"strings"
)

/**
Goes trough the poot shop and generate names

-> group name
    -> group name
		-> seed
	      -> seed name
    	  -> seed Url
          -> Seed Pic
          -> Seed Climate / region grown from  / origin (Free Text)


*/
type Location struct {
	Loc string `xml:"loc"`
}

type UrlSet struct {
	Locations []Location `xml:"url"`
}

func LoadXml() error {
	bytes, err := ioutil.ReadFile("./sitemap.xml")
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	db, err := ent.Open("postgres", common.PostgresDSN)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	if err := db.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	var s UrlSet
	xml.Unmarshal(bytes, &s)

	type Elem struct {
		Children map[string]*Elem
		Name     string
		Url  string
	}

	root := &Elem{
		Children: make(map[string]*Elem),
		Name:     "root",
	}

	for _, loc := range s.Locations {
		parts := strings.Split(strings.Split(loc.Loc, "https://www.lepotagerdesante.com/")[1], "/")

		current := root

		lastIndex := len(parts)-1
		for index, partName := range parts {
			if _, ok := current.Children[partName]; !ok {
				current.Children[partName] = &Elem{
					Children: make(map[string]*Elem),
					Name:     partName,
				}
			}
			if index == lastIndex {
				current.Children[partName].Url = loc.Loc
			}
			current = current.Children[partName]
		}

	}
	spew.Dump(root.Children["boutique"].Children["frutiers"])

	fmt.Println()




	categories := []string{"legumes-racines", "aromatique", "frutiers", "legumes-feuilles", "legumes-fruits"}
	for _, category := range categories {
		elem, ok := root.Children["boutique"].Children[category]
		if !ok {
			panic(category)
		}

		top, err := db.Plant.Query().Where(plant.Name(elem.Name)).WithPlants().Only(context.Background())
		if err != nil {
			top, err = db.Plant.Create().SetName(elem.Name).Save(context.Background())
			if err !=nil {
				panic(err)
			}
		}

		fmt.Println("=====")
		fmt.Println(elem.Name)
		fmt.Println("=====")
		for _, sub := range elem.Children {
			if strings.HasSuffix(sub.Name, ".html") {

				pl, err := db.PlantSpecie.Query().Where(plantspecie.Name(sub.Name)).Only(context.Background())
				if err != nil {
					pl, err = db.PlantSpecie.Create().SetName(sub.Name).Save(context.Background())
					if err !=nil {
						panic(err)
					}
				}
				pl.Update().SetURL(sub.Url).Save(context.Background())
				_, err = top.Update().AddSpecies(pl).Save(context.Background())
				if err !=nil {
					fmt.Println(err)
				}

				fmt.Println("   - PRODUCT - ", sub.Name)
			} else {
				subplant, err := db.Plant.Query().Where(plant.Name(sub.Name)).WithPlants().Only(context.Background())
				if err != nil {
					subplant, err = db.Plant.Create().SetName(sub.Name).Save(context.Background())
					if err !=nil {
						panic(err)
					}
				}

				_, err = top.Update().AddPlants(subplant).Save(context.Background())
				if err !=nil {
					fmt.Println(err)
				}
				fmt.Println("   - ", sub.Name)
				for _, subsub := range sub.Children {
					if strings.HasSuffix(subsub.Name, ".html") {
						fmt.Println("   - PRODUCT ", subsub.Name)

						pl, err := db.PlantSpecie.Query().Where(plantspecie.Name(subsub.Name)).Only(context.Background())
						if err != nil {
							pl, err = db.PlantSpecie.Create().SetName(subsub.Name).Save(context.Background())
							if err !=nil {
								panic(err)
							}
						}
						pl.Update().SetURL(subsub.Url).Save(context.Background())
						_, err =subplant.Update().AddSpecies(pl).Save(context.Background())
						if err !=nil {
							fmt.Println(err)
						}
					} else {

						fmt.Println("   - WEIRDOOOOO", subsub.Name)
					}
				}
			}

		}

	}

	fmt.Println("=====")

	for cat := range root.Children["boutique"].Children {
		fmt.Println(cat)
	}

	fmt.Println("=====")

	tomates, err := db.Plant.Query().Where(plant.Name("tomates")).Only(context.Background())
	if err !=nil {
		tomates, err = db.Plant.Create().SetName("tomates").Save(context.Background())
		if err !=nil {
			panic(err)
		}
	}

	legumesFruits, err := db.Plant.Query().Where(plant.Name("legumes-fruits")).Only(context.Background())
	if err !=nil {
		panic(err)
	}
	legumesFruits.Update().AddPlants(tomates).Save(context.Background())



	for cat := range root.Children["boutique"].Children["tomates"].Children {

		subTomate, err := db.Plant.Query().Where(plant.Name(cat)).Only(context.Background())
		if err !=nil {
			subTomate, err = db.Plant.Create().SetName(cat).Save(context.Background())
			if err !=nil {
				panic(err)
			}
		}
		tomates.Update().AddPlants(subTomate).Save(context.Background())

		for  sub, data := range root.Children["boutique"].Children["tomates"].Children[cat].Children {

			if strings.HasSuffix(sub, ".html") {
				semi, err := db.PlantSpecie.Query().Where(plantspecie.Name(sub)).Only(context.Background())
				if err !=nil {
					semi, err = db.PlantSpecie.Create().SetName(sub).SetURL(data.Url).Save(context.Background())
					if err !=nil {
						panic(err)
					}
				}
				subTomate.Update().AddSpecies(semi).Save(context.Background())
			}
		}

		fmt.Println(cat)
	}
	//tomates
	os.Exit(1)


	return nil
}

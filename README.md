# Welcome to PermaLib

The backend of the Perma-culture Knowledge Library Service.

### install deps

```bash
GO111MODULE=on go mod download
```

### Generate Ent

It uses facebook `ent`, https://github.com/facebook/ent

```
go get entgo.io/ent/cmd/ent
GO111MODULE=on go generate ./ent
```
### Start the web server:

```
GO111MODULE=on revel run
```

## Help

* The [Entity framework](https://github.com/facebook/ent)
* The [Getting Started with Revel](http://revel.github.io/tutorial/gettingstarted.html).
* The [Revel guides](http://revel.github.io/manual/index.html).
* The [Revel sample apps](http://revel.github.io/examples/index.html).
* The [API documentation](https://godoc.org/github.com/revel/revel).

